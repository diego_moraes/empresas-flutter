# APP

A version of empresas-flutter to ioasys test.

## Demo

https://drive.google.com/file/d/1aL0LFC0aVs-evtloPBf9-YRdaQZEhtHH/view?usp=sharing

## Code Snippets

### To update splash screen:
- Set flutter_native_splash properties in pubspec.yaml
- Execute following command
```shell
$ flutter pub pub run flutter_native_splash:create
```

### To update laucher icon:
```shell
$ flutter packages pub run flutter_launcher_icons:main
```