import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ImageHelper {
  static Image getCachedNetwork({
    BuildContext context,
    String url,
    BoxFit fit = BoxFit.none,
  }) {
    try {
      if (url == null || (url != null && url.isEmpty)) {
        return null;
      }

      CachedNetworkImage(
          placeholder: (context, url) => CircularProgressIndicator(),
          imageUrl: url);

      return Image(
        image: CachedNetworkImageProvider(url),
        fit: fit,
      );
    } catch (error) {
      return null;
    }
  }
}
