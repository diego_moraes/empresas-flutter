import 'package:flutter/material.dart';

class CircularLoading extends StatelessWidget {
  final double scale;

  const CircularLoading({
    Key key,
    this.scale = 1.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Transform.scale(
      scale: scale,
      child: Center(
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: [
            SizedBox(
              width: 47,
              height: 47,
              child: CircularProgressIndicator(
                strokeWidth: 3.0,
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Color.fromRGBO(251, 219, 231, 1),
                ),
              ),
            ),
            SizedBox(
              width: 72,
              height: 72,
              child: CircularProgressIndicator(
                strokeWidth: 3.0,
                valueColor: new AlwaysStoppedAnimation<Color>(
                  Color.fromRGBO(251, 219, 231, 1),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
