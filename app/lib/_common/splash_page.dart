import 'package:flutter/material.dart';

class SplashPage extends StatelessWidget {
  final Image splash;

  const SplashPage({
    Key key,
    @required this.splash,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: splash,
    );
  }
}
