import 'dart:io';

import 'package:app/_common/splash_page.dart';
import 'package:app/company/company_page.dart';
import 'package:app/authentication/bloc/authentication_bloc.dart';
import 'package:app/authentication/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class App extends StatefulWidget {
  const App({
    Key key,
  }) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  Image _splash;

  @override
  void initState() {
    super.initState();
    _splash = Image.asset(
      'assets/images/splash.png',
      fit: Platform.isAndroid ? BoxFit.fill : BoxFit.cover,
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(_splash.image, context);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'Rubik',
        appBarTheme: AppBarTheme(
          brightness: Brightness.light,
        ),
      ),
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          builder: (context, state) {
        if (state is AuthenticationInitialState ||
            state is UnauthenticatedState) {
          return LoginPage(state: state);
        }
        if (state is AuthenticatedState) {
          return CompanyPage(auth: state.auth);
        }
        return SplashPage(splash: _splash);
      }),
    );
  }
}
