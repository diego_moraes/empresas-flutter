import 'package:flutter/material.dart';

class LoginPageData extends InheritedWidget {
  final ValueChanged<bool> isTyping;

  LoginPageData({
    Key key,
    @required this.isTyping,
    @required Widget child,
  }) : super(key: key, child: child);

  static LoginPageData of(BuildContext context) =>
      context.dependOnInheritedWidgetOfExactType<LoginPageData>();

  @override
  bool updateShouldNotify(LoginPageData oldWidget) {
    return oldWidget.isTyping != isTyping;
  }
}
