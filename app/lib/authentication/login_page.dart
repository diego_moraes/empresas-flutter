import 'package:app/authentication/login_form.dart';
import 'package:app/authentication/login_header.dart';
import 'package:app/authentication/login_page_data.dart';
import 'package:flutter/material.dart';
import 'package:app/authentication/bloc/authentication_bloc.dart';

class LoginPage extends StatefulWidget {
  final AuthenticationState state;

  const LoginPage({
    Key key,
    @required this.state,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isTyping;

  @override
  void initState() {
    super.initState();
    _isTyping = false;
  }

  void onTypingChange(bool newValue) {
    if (_isTyping != newValue) {
      setState(() {
        _isTyping = newValue;
        print("Login isTyping: $_isTyping");
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return LoginPageData(
      isTyping: onTypingChange,
      child: Scaffold(
        appBar: LoginHeader(isCollapsed: _isTyping),
        body: LoginForm(state: widget.state),
      ),
    );
  }
}
