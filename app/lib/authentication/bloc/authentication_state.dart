part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationState {}

class AuthenticationInitialState extends AuthenticationState {}

class AuthenticatedState extends AuthenticationState {
  final Authentication auth;

  AuthenticatedState(this.auth);
}

class UnauthenticatedState extends AuthenticationState {}
