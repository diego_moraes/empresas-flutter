import 'package:app/authentication/model/authentication_model.dart';
import 'package:app/authentication/model/authentication_repository.dart';
import 'package:disposable_provider/disposable_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState>
    with Disposable {
  final AuthenticationRepository _repository = AuthenticationRepository();

  PublishSubject<bool> _isLoadingController;
  Stream<bool> get isLoadingStream => _isLoadingController.stream;

  AuthenticationBloc(AuthenticationState initialState) : super(initialState) {
    _isLoadingController = PublishSubject<bool>();
  }

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is LoginEvent) {
      yield await doLogin(
        email: event.email,
        password: event.password,
      );
    } else if (event is LoginEvent) {
      await doLogout();
      yield UnauthenticatedState();
    } else {
      yield AuthenticationInitialState();
    }
  }

  Future<AuthenticationState> doLogin({
    @required String email,
    @required String password,
  }) async {
    try {
      _isLoadingController.add(true);
      final auth = await _repository.doLogin(
        email: email,
        password: password,
      );
      if (auth != null) {
        _repository.persistAuthentication(auth);
      }
      return AuthenticatedState(auth);
    } catch (error) {
      return UnauthenticatedState();
    } finally {
      _isLoadingController.add(false);
    }
  }

  Future<AuthenticationState> doLogout() async {
    try {
      await _repository.doLogout();
      return UnauthenticatedState();
    } catch (error) {
      return AuthenticationInitialState();
    }
  }

  @override
  void dispose() {
    _isLoadingController.drain().then((value) => _isLoadingController.close());
  }
}
