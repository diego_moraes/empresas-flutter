import 'dart:ui';

import 'package:app/_common/circular_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:app/authentication/bloc/authentication_bloc.dart';
import 'package:app/authentication/login_page_data.dart';

class LoginForm extends StatefulWidget {
  final AuthenticationState state;

  const LoginForm({
    Key key,
    @required this.state,
  }) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  AuthenticationBloc _authBloc;
  GlobalKey<FormState> _formKey;
  TextEditingController _emailController;
  TextEditingController _passwordController;
  FocusNode _emailFocus;
  FocusNode _passwordFocus;
  bool _showPassword;
  bool _isValidForm;

  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthenticationBloc>(context);
    _formKey = GlobalKey<FormState>();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    _emailFocus = FocusNode();
    _passwordFocus = FocusNode();
    _showPassword = false;
    _isValidForm = true;

    _emailController.addListener(_updateIsTyping);
    _passwordController.addListener(_updateIsTyping);
    _emailFocus.addListener(_updateIsTyping);
    _passwordFocus.addListener(_updateIsTyping);
  }

  void _updateIsTyping() {
    if (_emailController.text.isNotEmpty ||
        _passwordController.text.isNotEmpty ||
        _emailFocus.hasFocus ||
        _passwordFocus.hasFocus) {
      LoginPageData.of(context).isTyping(true);
    } else {
      LoginPageData.of(context).isTyping(false);
    }
  }

  @override
  void didUpdateWidget(LoginForm oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.state != widget.state) {
      _isValidForm = _formKey.currentState.validate();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Form(
          key: _formKey,
          child: ListView(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 4.0),
                child: Text(
                  "Email",
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Color.fromRGBO(102, 102, 102, 1),
                  ),
                ),
              ),
              TextFormField(
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                controller: _emailController,
                focusNode: _emailFocus,
                textInputAction: TextInputAction.next,
                style: TextStyle(
                    fontSize: 16.0, color: Color.fromRGBO(0, 0, 0, 1)),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Color.fromRGBO(245, 245, 245, 1),
                  border: widget.state is UnauthenticatedState
                      ? OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red))
                      : InputBorder.none,
                  errorStyle: TextStyle(height: 0),
                  suffixIcon: !_isValidForm && _emailController.text.isNotEmpty
                      ? GestureDetector(
                          onTap: () {
                            setState(() {
                              _emailController.text = "";
                              _isValidForm = _formKey.currentState.validate();
                            });
                          },
                          child: Icon(
                            Icons.cancel,
                            color: Colors.red,
                          ),
                        )
                      : SizedBox(),
                ),
                onFieldSubmitted: (value) {
                  FocusScope.of(context).requestFocus(_passwordFocus);
                },
                validator: (value) {
                  if (value.isEmpty) return null;
                  return widget.state is UnauthenticatedState ? "" : null;
                },
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0, bottom: 4.0),
                child: Text(
                  "Senha",
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Color.fromRGBO(102, 102, 102, 1),
                  ),
                ),
              ),
              TextFormField(
                autocorrect: false,
                controller: _passwordController,
                focusNode: _passwordFocus,
                textInputAction: TextInputAction.done,
                style: TextStyle(
                    fontSize: 16.0, color: Color.fromRGBO(0, 0, 0, 1)),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Color.fromRGBO(245, 245, 245, 1),
                  border: widget.state is UnauthenticatedState
                      ? OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red))
                      : InputBorder.none,
                  errorStyle: TextStyle(height: 0),
                  suffixIcon: !_isValidForm &&
                          _passwordController.text.isNotEmpty
                      ? GestureDetector(
                          onTap: () {
                            setState(() {
                              _passwordController.text = "";
                              _isValidForm = _formKey.currentState.validate();
                            });
                          },
                          child: Icon(
                            Icons.cancel,
                            color: Colors.red,
                          ),
                        )
                      : GestureDetector(
                          onTap: () {
                            setState(() {
                              _showPassword = !_showPassword;
                            });
                          },
                          child: Icon(
                            _showPassword
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.black54,
                          ),
                        ),
                ),
                obscureText: !_showPassword,
                onFieldSubmitted: (value) {
                  _passwordFocus.unfocus();
                },
                validator: (value) {
                  if (value.isEmpty) return null;
                  return widget.state is UnauthenticatedState ? "" : null;
                },
              ),
              !_isValidForm
                  ? Padding(
                      padding: const EdgeInsets.only(top: 4.0),
                      child: Text(
                        "Credenciais incorretas",
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 12.0,
                        ),
                      ),
                    )
                  : SizedBox(),
              Padding(
                padding: EdgeInsets.only(top: 40.0, left: 5.0, right: 5.0),
                child: ButtonTheme(
                  height: 48.0,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.resolveWith(
                            (_) => Color.fromRGBO(224, 30, 105, 1)),
                        textStyle: MaterialStateProperty.resolveWith(
                            (_) => TextStyle(color: Colors.white)),
                        shape: MaterialStateProperty.resolveWith((_) {
                          return RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          );
                        })),
                    onPressed: () {
                      _authBloc.add(
                        LoginEvent(
                          email: _emailController.text,
                          password: _passwordController.text,
                        ),
                      );
                    },
                    child: Text(
                      "ENTRAR",
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
        StreamBuilder(
            stream: _authBloc.isLoadingStream,
            initialData: null,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return CircularLoading();
              }
              return SizedBox();
            }),
      ],
    );
  }

  @override
  void dispose() {
    _authBloc.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _emailFocus.dispose();
    _passwordFocus.dispose();
    super.dispose();
  }
}
