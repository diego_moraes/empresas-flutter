import 'package:flutter/material.dart';

class LoginHeader extends StatefulWidget implements PreferredSizeWidget {
  static const EXPANDED = 240.0;
  static const COLLAPSED = 140.0;
  final bool isCollapsed;

  const LoginHeader({
    Key key,
    @required this.isCollapsed,
  }) : super(key: key);

  @override
  _LoginHeaderState createState() => _LoginHeaderState();

  @override
  Size get preferredSize => Size.fromHeight(LoginHeader.EXPANDED);
}

class _LoginHeaderState extends State<LoginHeader> {
  Image _backExpanded;
  Image _backCollapsed;

  @override
  void initState() {
    super.initState();

    _backExpanded = Image.asset(
      'assets/images/back2.png',
      fit: BoxFit.fill,
      width: double.infinity,
      alignment: Alignment.topCenter,
    );

    _backCollapsed = Image.asset(
      'assets/images/back2_collapsed.png',
      fit: BoxFit.fill,
      width: double.infinity,
      alignment: Alignment.topCenter,
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(_backExpanded.image, context);
    precacheImage(_backCollapsed.image, context);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<double>(
        future: !widget.isCollapsed
            ? Future<double>.value(LoginHeader.EXPANDED)
            : Future<double>.value(LoginHeader.COLLAPSED),
        initialData: LoginHeader.EXPANDED,
        builder: (context, snapshot) {
          return !widget.isCollapsed
              ? ClipRRect(
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      AnimatedContainer(
                          duration: Duration(seconds: 1),
                          width: double.infinity,
                          height: snapshot.data,
                          child: _backExpanded),
                      Padding(
                        padding: EdgeInsets.only(top: 70.0),
                        child: Text(
                          "Seja bem vindo ao empresas!",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                  borderRadius: new BorderRadius.vertical(
                      bottom: new Radius.elliptical(
                          MediaQuery.of(context).size.width, 150.0)),
                )
              : ClipRRect(
                  child: AnimatedContainer(
                      duration: Duration(seconds: 1),
                      width: double.infinity,
                      height: snapshot.data,
                      child: _backCollapsed),
                  borderRadius: new BorderRadius.vertical(
                      bottom: new Radius.elliptical(
                          MediaQuery.of(context).size.width, 150.0)),
                );
        });
  }
}
