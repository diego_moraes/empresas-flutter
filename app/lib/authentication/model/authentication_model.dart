class Authentication {
  final String accessToken;
  final String client;
  final String uid;

  Authentication({
    this.accessToken,
    this.client,
    this.uid,
  });

  Authentication.fromJson(Map<String, List<String>> json)
      : accessToken = json['access-token'].first,
        client = json['client'].first,
        uid = json['uid'].first;

  Map<String, dynamic> toJson() => {
        'access-token': accessToken,
        'client': client,
        'uid': uid,
      };
}
