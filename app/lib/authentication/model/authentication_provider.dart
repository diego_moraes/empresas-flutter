import 'dart:convert';

import 'package:app/authentication/model/authentication_exception.dart';
import 'package:app/authentication/model/authentication_model.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthenticationProvider {
  final _dio = Dio();

  Future<Authentication> doLogin(email, password) async {
    try {
      Response response = await _dio.post(
          'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
          data: {'email': email, 'password': password},
          options: Options(
              headers: {'Content-Type': 'application/json;charset=UTF-8'}));

      final authModel = Authentication.fromJson(response.headers.map);

      return authModel;
    } on DioError catch (error) {
      throw AuthenticationException("Authentication Failed ${error.message}");
    } catch (error) {
      throw error;
    }
  }

  Future<void> persistAuthentication(Authentication auth) async {
    try {
      final prefs = await SharedPreferences.getInstance();
      await prefs.setString('auth', jsonEncode(auth.toJson()));
    } catch (error) {
      throw error;
    }
  }

  Future<Authentication> getAuthentication() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      final json = await prefs.get('auth');
      return json != null ? Authentication.fromJson(jsonDecode(json)) : null;
    } catch (error) {
      throw error;
    }
  }

  Future<void> doLogout() async {
    try {
      final prefs = await SharedPreferences.getInstance();
      await prefs.remove('auth');
    } catch (error) {
      throw error;
    }
  }
}
