import 'package:app/authentication/model/authentication_model.dart';
import 'package:app/authentication/model/authentication_provider.dart';
import 'package:flutter/material.dart';

class AuthenticationRepository {
  final AuthenticationProvider _provider = AuthenticationProvider();
  Authentication _auth;

  static AuthenticationRepository _instance;
  factory AuthenticationRepository() {
    _instance ??= AuthenticationRepository._internalConstructor();
    return _instance;
  }
  AuthenticationRepository._internalConstructor();

  Future<Authentication> doLogin({
    @required String email,
    @required String password,
  }) async {
    _auth = await _provider.doLogin(email, password);
    return _auth;
  }

  Future<void> persistAuthentication(Authentication login) async {
    await _provider.persistAuthentication(_auth);
    return;
  }

  Future<Authentication> getAuthentication() async {
    if (_auth != null) {
      return _auth;
    } else {
      _auth = await _provider.getAuthentication();
      return _auth;
    }
  }

  Future<void> doLogout() async {
    await _provider.doLogout();
    return;
  }
}
