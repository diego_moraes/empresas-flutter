class AuthenticationException implements Exception {
  final String cause;

  AuthenticationException(this.cause);
}
