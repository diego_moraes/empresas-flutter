import 'package:app/app.dart';
import 'package:app/authentication/bloc/authentication_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(
    BlocProvider<AuthenticationBloc>(
      create: (context) {
        return AuthenticationBloc(AuthenticationInitialState());
      },
      child: App(),
    ),
  );
}
