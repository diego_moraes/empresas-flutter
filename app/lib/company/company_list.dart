import 'package:app/_common/circular_loading.dart';
import 'package:app/company/bloc/company_bloc.dart';
import 'package:app/company/company_card.dart';
import 'package:app/company/model/company_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CompanyList extends StatefulWidget {
  const CompanyList({
    Key key,
  }) : super(key: key);

  @override
  _CompanyListState createState() => _CompanyListState();
}

class _CompanyListState extends State<CompanyList> {
  CompanyBloc _companyBloc;

  @override
  void initState() {
    super.initState();
    _companyBloc = BlocProvider.of<CompanyBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        StreamBuilder(
            stream: _companyBloc.companiesStream,
            initialData: null,
            builder:
                (BuildContext context, AsyncSnapshot<List<Company>> snapshot) {
              if (snapshot.hasData) {
                if (snapshot.data.length == 0) {
                  return Center(
                      child: Text(
                    "Nenhum resultado encontrado",
                    style: TextStyle(
                      fontSize: 18,
                      color: Color.fromRGBO(102, 102, 102, 1),
                    ),
                  ));
                }

                return Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0, top: 8.0),
                      child: Text(
                          snapshot.data.length > 1
                              ? "${snapshot.data.length} resultados encontrados"
                              : "${snapshot.data.length} resultado encontrado",
                          style: TextStyle(
                            fontSize: 14,
                            color: Color.fromRGBO(102, 102, 102, 1),
                          )),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 15.0, right: 15.0, top: 40.0),
                      child: ListView.builder(
                          itemCount: snapshot.data.length ?? 0,
                          itemBuilder: (context, i) {
                            return CompanyCard(company: snapshot.data[i]);
                          }),
                    ),
                  ],
                );
              } else {
                return SizedBox();
              }
            }),
        StreamBuilder(
            stream: _companyBloc.isLoadingStream,
            initialData: null,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              if (snapshot.hasData && snapshot.data) {
                return CircularLoading();
              }
              return SizedBox();
            }),
      ],
    );
  }

  @override
  void dispose() {
    _companyBloc.dispose();
    super.dispose();
  }
}
