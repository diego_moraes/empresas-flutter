import 'package:app/authentication/model/authentication_model.dart';
import 'package:app/company/model/company_model.dart';
import 'package:app/company/model/company_provider.dart';
import 'package:flutter/material.dart';

class CompanyRepository {
  final CompanyProvider _provider = CompanyProvider();

  Future<List<Company>> doSearch({
    @required Authentication auth,
    @required String searchKey,
  }) async {
    final result = await _provider.doSearch(auth, searchKey);
    return result;
  }
}
