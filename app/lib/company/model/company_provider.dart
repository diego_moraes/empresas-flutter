import 'package:app/authentication/model/authentication_exception.dart';
import 'package:app/authentication/model/authentication_model.dart';
import 'package:app/company/model/company_model.dart';
import 'package:dio/dio.dart';

class CompanyProvider {
  final _dio = Dio();

  Future<List<Company>> doSearch(Authentication auth, String searchKey) async {
    try {
      Response response = await _dio.get(
          'https://empresas.ioasys.com.br/api/v1/enterprises?name=${Uri.encodeFull(searchKey)}',
          options: Options(headers: {
            'Content-Type': 'application/json;charset=UTF-8',
            'access-token': auth.accessToken,
            'client': auth.client,
            'uid': auth.uid,
          }));

      List<Company> list = [];
      for (var item in response.data['enterprises']) {
        list.add(Company.fromJson(item));
      }

      return list;
    } on DioError catch (error) {
      if (error.response.statusCode == 401) {
        throw AuthenticationException("Authentication Failed ${error.message}");
      }
      throw error;
    } catch (error) {
      throw error;
    }
  }
}
