class Company {
  final int id;
  final String name;
  final String description;
  final String photo;

  Company(
    this.id,
    this.name,
    this.description,
    this.photo,
  );

  Company.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['enterprise_name'],
        description = json['description'],
        photo = "https://empresas.ioasys.com.br${json['photo']}";

  Map<String, dynamic> toJson() => {
        'id': id,
        'enterprise_name': name,
        'description': description,
        'photo': photo,
      };
}
