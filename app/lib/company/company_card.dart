import 'package:app/company/company_detail_page.dart';
import 'package:app/company/model/company_model.dart';
import 'package:flutter/material.dart';
import 'package:random_color/random_color.dart';

class CompanyCard extends StatelessWidget {
  final Company company;

  const CompanyCard({
    Key key,
    @required this.company,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Color color = RandomColor().randomColor(
      colorSaturation: ColorSaturation.lowSaturation,
      colorBrightness: ColorBrightness.light,
    );

    return Card(
      elevation: 1.0,
      color: color,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      child: TextButton(
        child: Container(
          height: 120,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Flexible(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text.rich(
                        TextSpan(
                          text: company.name,
                        ),
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 4,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CompanyDetailPage(
                  company: company,
                  color: color,
                ),
              ));
        },
      ),
    );
  }
}
