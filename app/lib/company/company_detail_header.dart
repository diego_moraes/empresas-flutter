import 'package:app/company/model/company_model.dart';
import 'package:flutter/material.dart';

class CompanyDetailHeader extends StatelessWidget
    implements PreferredSizeWidget {
  final Company company;

  const CompanyDetailHeader({
    Key key,
    @required this.company,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(70);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0.0,
      backgroundColor: Colors.white,
      leading: Builder(
        builder: (BuildContext context) {
          return Padding(
            padding: EdgeInsets.only(left: 15.0, top: 15.0),
            child: Ink(
              decoration: ShapeDecoration(
                color: Color.fromRGBO(245, 245, 245, 1),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(4.0),
                ),
              ),
              child: IconButton(
                color: Color.fromRGBO(245, 245, 245, 1),
                icon: Icon(
                  Icons.arrow_back,
                  color: Color.fromRGBO(224, 30, 105, 1),
                  size: 25,
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          );
        },
      ),
      titleSpacing: 0.0,
      title: Padding(
        padding: EdgeInsets.only(top: 15.0, left: 5.0, right: 5.0),
        child: Text.rich(
          TextSpan(
            text: company.name,
          ),
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}
