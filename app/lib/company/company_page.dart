import 'package:app/authentication/model/authentication_model.dart';
import 'package:app/company/bloc/company_bloc.dart';
import 'package:app/company/company_header.dart';
import 'package:app/company/company_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CompanyPage extends StatelessWidget {
  final Authentication auth;

  const CompanyPage({
    Key key,
    @required this.auth,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocProvider<CompanyBloc>(
        create: (context) {
          return CompanyBloc(CompanyInitialState(), auth);
        },
        child: Scaffold(
          appBar: CompanyHeader(),
          body: CompanyList(),
        ),
      ),
    );
  }
}
