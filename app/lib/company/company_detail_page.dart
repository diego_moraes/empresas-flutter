import 'package:app/company/company_detail_body.dart';
import 'package:app/company/company_detail_header.dart';
import 'package:app/company/model/company_model.dart';
import 'package:flutter/material.dart';

class CompanyDetailPage extends StatelessWidget {
  final Company company;
  final Color color;

  const CompanyDetailPage({
    Key key,
    @required this.company,
    @required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CompanyDetailHeader(
        company: company,
      ),
      body: CompanyDetailBody(
        company: company,
        color: color,
      ),
    );
  }
}
