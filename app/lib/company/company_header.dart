import 'package:app/company/bloc/company_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CompanyHeader extends StatefulWidget implements PreferredSizeWidget {
  static const EXPANDED = 230.0;
  static const COLLAPSED = 100.0;

  const CompanyHeader({
    Key key,
  }) : super(key: key);

  @override
  _CompanyHeaderState createState() => _CompanyHeaderState();

  @override
  Size get preferredSize => Size.fromHeight(CompanyHeader.EXPANDED);
}

class _CompanyHeaderState extends State<CompanyHeader> {
  CompanyBloc _companyBloc;
  Image _backExpanded;
  Image _backCollapsed;
  TextEditingController _searchController;
  FocusNode _searchFocus;
  bool _isTyping;

  @override
  void initState() {
    super.initState();
    _companyBloc = BlocProvider.of<CompanyBloc>(context);
    _searchController = TextEditingController();
    _searchFocus = FocusNode();
    _isTyping = false;

    _backExpanded = Image.asset(
      'assets/images/back3.png',
      fit: BoxFit.fill,
      width: double.infinity,
      alignment: Alignment.topCenter,
    );

    _backCollapsed = Image.asset(
      'assets/images/back3_collapsed.png',
      fit: BoxFit.fill,
      width: double.infinity,
      alignment: Alignment.topCenter,
    );

    _searchController.addListener(_updateIsTyping);
    _searchFocus.addListener(_updateIsTyping);
  }

  void _updateIsTyping() {
    if (_searchController.text.isNotEmpty || _searchFocus.hasFocus) {
      if (!_isTyping) {
        setState(() {
          _isTyping = true;
          print("Search isTyping: $_isTyping");
        });
      }
    } else {
      if (_isTyping) {
        setState(() {
          _isTyping = false;
          print("Search isTyping: $_isTyping");
        });
      }
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    precacheImage(_backExpanded.image, context);
    precacheImage(_backCollapsed.image, context);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<double>(
        future: !_isTyping
            ? Future<double>.value(CompanyHeader.EXPANDED)
            : Future<double>.value(CompanyHeader.COLLAPSED),
        initialData: CompanyHeader.EXPANDED,
        builder: (context, snapshot) {
          return AnimatedContainer(
              duration: Duration(seconds: 1),
              width: double.infinity,
              height: snapshot.data,
              child: Stack(
                children: [
                  !_isTyping ? _backExpanded : _backCollapsed,
                  Padding(
                    padding: !_isTyping
                        ? EdgeInsets.only(left: 15.0, right: 15.0, top: 155)
                        : EdgeInsets.only(left: 15.0, right: 15.0, top: 35),
                    child: TextFormField(
                      autocorrect: false,
                      keyboardType: TextInputType.text,
                      controller: _searchController,
                      focusNode: _searchFocus,
                      textInputAction: TextInputAction.search,
                      style: TextStyle(
                          fontSize: 18.0,
                          color: Color.fromRGBO(102, 102, 102, 0.7)),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Color.fromRGBO(245, 245, 245, 1),
                        border: new OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                            const Radius.circular(4.0),
                          ),
                        ),
                        hintText: "Pesquise por empresa",
                        hintStyle: TextStyle(
                            fontSize: 18.0,
                            color: Color.fromRGBO(102, 102, 102, 0.7)),
                        prefixIcon: Icon(
                          Icons.search,
                          color: Colors.black54,
                          size: 30,
                        ),
                      ),
                      onFieldSubmitted: (value) {
                        if (value.isNotEmpty) {
                          _companyBloc
                              .add(SearchCompanyEvent(searchKey: value));
                        }
                      },
                    ),
                  ),
                ],
              ));
        });
  }

  @override
  void dispose() {
    _companyBloc.dispose();
    _searchController.dispose();
    _searchFocus.dispose();
    super.dispose();
  }
}
