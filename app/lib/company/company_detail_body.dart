import 'package:app/_common/helpers/image_helper.dart';
import 'package:app/company/model/company_model.dart';
import 'package:flutter/material.dart';

class CompanyDetailBody extends StatelessWidget {
  final Company company;
  final Color color;

  const CompanyDetailBody({
    Key key,
    @required this.company,
    @required this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 150,
          width: double.infinity,
          child: ImageHelper.getCachedNetwork(
            context: context,
            url: company.photo,
            fit: BoxFit.cover,
          ),
        ),
        Expanded(
          flex: 1,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 25.0),
              child: Text(
                company.description,
                style: TextStyle(
                  fontSize: 18,
                  color: Colors.black,
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
