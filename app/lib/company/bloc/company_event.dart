part of 'company_bloc.dart';

abstract class CompanyEvent {
  const CompanyEvent();

  List<Object> get props => [];
}

class SearchCompanyEvent extends CompanyEvent {
  final String searchKey;

  const SearchCompanyEvent({
    @required this.searchKey,
  });

  @override
  List<Object> get props => [searchKey];

  @override
  String toString() {
    return 'SearchCompanyEvent { searchKey: $searchKey }';
  }
}
