import 'package:app/authentication/bloc/authentication_bloc.dart';
import 'package:app/authentication/model/authentication_exception.dart';
import 'package:app/authentication/model/authentication_model.dart';
import 'package:app/company/model/company_model.dart';
import 'package:app/company/model/company_repository.dart';
import 'package:disposable_provider/disposable_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';

part 'company_event.dart';
part 'company_state.dart';

class CompanyBloc extends Bloc<CompanyEvent, CompanyState> with Disposable {
  final CompanyRepository _repository = CompanyRepository();
  final Authentication _auth;

  PublishSubject<bool> _isLoadingController;
  Stream<bool> get isLoadingStream => _isLoadingController.stream;

  PublishSubject<List<Company>> _listController;
  Stream<List<Company>> get companiesStream => _listController.stream;

  CompanyBloc(
    CompanyState initialState,
    this._auth,
  ) : super(initialState) {
    _isLoadingController = PublishSubject<bool>();
    _listController = PublishSubject<List<Company>>();
  }

  @override
  Stream<CompanyState> mapEventToState(CompanyEvent event) async* {
    if (event is SearchCompanyEvent) {
      yield await doSearch(
        searchKey: event.searchKey,
      );
    } else {
      yield CompanyInitialState();
    }
  }

  Future<AuthenticationState> doSearch({
    @required String searchKey,
  }) async {
    try {
      _isLoadingController.add(true);
      final result = await _repository.doSearch(
        auth: _auth,
        searchKey: searchKey,
      );
      _listController.add(result);
      return CompanySearchDoneState();
    } on AuthenticationException {
      return UnauthenticatedState();
    } catch (error) {
      return CompanySearchErrorState();
    } finally {
      _isLoadingController.add(false);
    }
  }

  @override
  void dispose() {
    _isLoadingController.drain().then((value) => _isLoadingController.close());
    _listController.drain().then((value) => _listController.close());
  }
}
