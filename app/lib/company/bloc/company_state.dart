part of 'company_bloc.dart';

@immutable
abstract class CompanyState extends AuthenticationState {}

class CompanyInitialState extends CompanyState {}

class CompanySearchDoneState extends CompanyState {}

class CompanySearchErrorState extends CompanyState {}
